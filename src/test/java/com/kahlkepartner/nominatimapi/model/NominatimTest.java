package com.kahlkepartner.nominatimapi.model;

import com.kahlkepartner.nominatimapi.responses.SearchResponse;
import com.kahlkepartner.nominatimapi.services.SearchService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest()
public class NominatimTest {
    @Autowired
    SearchService searchService;

    @Test
    public void searchTest() {
        Address address = new Address();

        address.setCity("Bannewitz");

        SearchResponse[] response = searchService.geocodeAddress(address);

        Assertions.assertEquals(response.length, 1);
        for (var res : response) {
            Assertions.assertNotNull(res.getLat());
            Assertions.assertNotNull(res.getLon());
            System.out.println(res);
        }
        System.out.println();

    }
}
