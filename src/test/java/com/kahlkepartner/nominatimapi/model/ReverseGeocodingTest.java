package com.kahlkepartner.nominatimapi.model;

import com.kahlkepartner.nominatimapi.services.ReverseSearchService;
import model.geometry.GeoJSONPoint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ReverseGeocodingTest {
    @Autowired
    ReverseSearchService reverseSearchService;

    @Test
    public void test() {
        GeoJSONPoint geoJSONPoint = GeoJSONPoint.fromLngLat(14.785849, 51.395922);
        var res = reverseSearchService.reverseGeocodeAddress(geoJSONPoint);
        Assertions.assertNotNull(res);
        GeoJSONPoint geoJSONPoint2 = GeoJSONPoint.fromLngLat(8.08113, 51.86638);
        var res2 = reverseSearchService.reverseGeocodeAddress(geoJSONPoint2);
        Assertions.assertNotNull(res2);
    }
}
