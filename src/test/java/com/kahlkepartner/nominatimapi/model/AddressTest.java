package com.kahlkepartner.nominatimapi.model;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;



@JsonTest
class AddressTest {
    @Autowired
    private JacksonTester<Address> json;

    @Test
    public void testSerialization() throws IOException {
        Address address = new Address();
        address.setCity("Biebelnheim");
        address.setCountry("Deutschland");
        address.setHouseNumber("9");
        address.setPostalCode("55234");
        address.setStreet("Gabsheimer Weg");

        var serialized = json.write(address);
        assertThat(serialized).extractingJsonPathStringValue("$.street").isEqualTo("9 Gabsheimer Weg");

    }

}
