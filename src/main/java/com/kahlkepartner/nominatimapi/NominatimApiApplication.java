package com.kahlkepartner.nominatimapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NominatimApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(NominatimApiApplication.class, args);
    }
}
