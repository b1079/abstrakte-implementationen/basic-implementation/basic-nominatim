package com.kahlkepartner.nominatimapi.exceptions;

import lombok.SneakyThrows;
import model.exeptions.GenericRoutingException;
import model.exeptions.ParsingException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

public class ApiErrorHandler implements ResponseErrorHandler {
    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        if (response.getRawStatusCode() != 200) {
            return true;
        }
        return false;
    }

    @SneakyThrows
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        if (response.getRawStatusCode() == 403)
            throw new ParsingException(response.getStatusText());
        throw new GenericRoutingException(response.getStatusText());
    }
}
