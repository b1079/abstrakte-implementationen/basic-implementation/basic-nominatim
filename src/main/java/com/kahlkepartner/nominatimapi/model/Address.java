package com.kahlkepartner.nominatimapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import interfaces.data.AddressType;
import lombok.Data;


@Data
/**
 * custom address object
 * example response
 * {"place_id":207220620,"licence":"Data © OpenStreetMap contributors,ODbL 1.0. https://osm.org/copyright",
 * "osm_type":"way","osm_id":539516717,
 * "lat":"48.46308315","lon":"9.76110315"
 * ,"place_rank":30,"category":"building","type":"yes","importance":0,
 * "addresstype":"building","name":null,"display_name":"15, Blaubeurer Straße, Berghülen, Bühlenhausen, Berghülen, Gemeindeverwaltungsverband Blaubeuren, Alb-Donau-Kreis, Baden-Württemberg, 89180, Deutschland",
 * "address":{"house_number":"15","road":"Blaubeurer Straße","residential":"Berghülen","village":"Bühlenhausen","municipality":"Gemeindeverwaltungsverband Blaubeuren","county":"Alb-Donau-Kreis","state":"Baden-Württemberg","postcode":"89180","country":"Deutschland","country_code":"de"},"boundingbox":["48.4630307","48.4631356","9.7610147","9.7611916"]}
 */
public class Address implements AddressType {
    @JsonProperty("road")
    String street = "";
    @JsonProperty("house_number")
    String houseNumber = "";
    String town;
    String residential;
    String village;
    String municipality;
    String county;
    String district;
    String city;
    String country = "";
    @JsonProperty("postcode")
    String postalCode = "";
    @JsonProperty("state")
    String state = "";
    String province;

    @JsonProperty("country_code")
    private String countryCode;

    @JsonProperty("street")
    public String getStreetWithHousenumber() {
        if (houseNumber == null || houseNumber.equals("")) {
            return street;
        }
        return houseNumber + " " + street;
    }

    public String getCity() {

        if (city != null)
            return city;
        if (town != null)
            return town;
        if (residential != null)
            return residential;
        if (village != null)
            return village;
        if (municipality != null)
            return municipality;
        return state;
    }


    public static Address fromAddressModel(interfaces.data.Address address) {
        Address addressObj = new Address();
        addressObj.setCountry(address.getCountry());
        addressObj.setHouseNumber(address.getHouseNumber());
        addressObj.setStreet(address.getStreet());
        addressObj.setCity(address.getCity());
        addressObj.setPostalCode(address.getPostalCode());
        addressObj.setState(address.getState());
        addressObj.setCountryCode(address.getCountry());
        return addressObj;
    }


}
