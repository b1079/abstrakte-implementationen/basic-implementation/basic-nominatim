package com.kahlkepartner.nominatimapi.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kahlkepartner.nominatimapi.model.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * search response from nominatim
 */
public class SearchResponse {
    @JsonProperty("place_id")
    String placeId;
    String license;
    @JsonProperty("osm_type")
    String osmType;
    @JsonProperty("boundingbox")
    List<String> boundingBox;
    String lat;
    String lon;
    @JsonProperty("display_name")
    String displayName;
    @JsonProperty("class")
    String responseClass;
    String type;
    double importance;
    String icon;
    Address address;
    @JsonProperty("extratags")
    Map<String, String> extraTags;


}
