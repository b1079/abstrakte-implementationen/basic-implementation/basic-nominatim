package com.kahlkepartner.nominatimapi.services;


import com.kahlkepartner.nominatimapi.exceptions.ApiErrorHandler;
import com.kahlkepartner.nominatimapi.responses.SearchResponse;
import model.geometry.GeoJSONPoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
@EnableConfigurationProperties(NominatimProperties.class)
@ConditionalOnExpression("${nominatim.enabled:true}")
/**
 * Reverse geocoder implementation in Nominatim
 */
public class ReverseSearchService {
    NominatimProperties nominatimProperties;

    public ReverseSearchService(NominatimProperties nominatimProperties) {
        this.nominatimProperties = nominatimProperties;
    }

    public SearchResponse reverseGeocodeAddress(GeoJSONPoint point) {
        HttpHeaders headers = new HttpHeaders();

        headers.setAccept(List.of(MediaType.ALL));

        RestTemplate restTemplate = new RestTemplate();

        restTemplate.setErrorHandler(new ApiErrorHandler());
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(nominatimProperties.getUrl() + "/reverse")
                .queryParam("lat", point.getLatitude())
                .queryParam("lon", point.getLongitude())
                // building level scope
                .queryParam("zoom", 18)
                .queryParam("adressdetails", 1)
                .queryParam("format", "jsonv2");

        HttpEntity<?> request = new HttpEntity<>(headers);

        HttpEntity<SearchResponse> response = restTemplate.exchange(
                builder.build(false).toUriString(),
                HttpMethod.GET,
                request,
                SearchResponse.class
        );

        return response.getBody();
    }

}
