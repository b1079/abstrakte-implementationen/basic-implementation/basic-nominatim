package com.kahlkepartner.nominatimapi.services;

import com.kahlkepartner.nominatimapi.exceptions.ApiErrorHandler;
import com.kahlkepartner.nominatimapi.responses.SearchResponse;
import com.kahlkepartner.nominatimapi.model.Address;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
@EnableConfigurationProperties(NominatimProperties.class)
@ConditionalOnExpression("${nominatim.enabled:true}")
/**
 * Geocoding Service for Nominatim
 */
public class SearchService {

    NominatimProperties nominatimProperties;

    public SearchService(NominatimProperties nominatimProperties) {
        this.nominatimProperties = nominatimProperties;
    }

    public SearchResponse[] geocodeAddress(Address address) {
        HttpHeaders headers = new HttpHeaders();

        headers.setAccept(List.of(MediaType.ALL));

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new ApiErrorHandler());
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(nominatimProperties.getUrl())
                .queryParam("city", address.getCity())
                .queryParam("street", address.getStreetWithHousenumber())
                .queryParam("country", address.getCountry())
                .queryParam("postalcode", address.getPostalCode())
                .queryParam("addressdetails", "1")
                .queryParam("format", "jsonv2");

        HttpEntity<?> request = new HttpEntity<>(headers);

        HttpEntity<SearchResponse[]> response = restTemplate.exchange(
                builder.build(false).toUriString(),
                HttpMethod.GET,
                request,
                SearchResponse[].class
        );

        return response.getBody();
    }

}
