package com.kahlkepartner.nominatimapi.services;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("nominatim")
/**
 * config file / settings file
 */
public class NominatimProperties {
    private String url = "http://localhost:5450";
}
